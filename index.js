require ('dotenv').config();

const PORT = process.env.PORT;
const bodyParser = require ('body-parser');
const fs = require ('fs');
const express = require ('express');

let file;

const loadFile = () => file = JSON.parse (fs.readFileSync(process.env.DB, 'utf-8'));


const app = express();

app.use(bodyParser.json({limit: "20mb"}))
app.use(bodyParser.urlencoded({}));

app.get('/', (req, res, next) => {
    loadFile();
    res.send(file);
}); 
app.get('/:id ', (res, req, next) => {
    loadFile();
    const result = file.find(obj => parseInt(obj._id) === parseInt(req.params.id));
    res.send(result);
});
app.listen(PORT, () => console.log(`Running on port  ${PORT}`));